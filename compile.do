exec >&2

redo-ifchange config.redo

command="go build"

. ./config.redo

if [ -n "$LDFLAGS" ]; then
    command="$command -ldflags '$LDFLAGS'"
fi

if [ -n "$GCFLAGS" ]; then
    command="$command -gcflags='$GCFLAGS'"
fi

if [ -n "$TAGS" ]; then
    command="$command -tags $TAGS"
fi

command="$command -o \"\$2\" \"\$1\""

NL='
'
environ=
for env in $ENV; do
    environ="${environ}export ${env}${NL}"
done

cat <<EOF >$3
#!/bin/sh
$environ
$command
EOF
chmod +x $3
