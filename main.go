package main

import (
	"bytes"
	"fmt"
	"io"
	"net/mail"
	"os"
	"path"

	flag "github.com/spf13/pflag"
	"gitlab.alpinelinux.org/kdaudt/gomaildrop/filter"
	"gitlab.alpinelinux.org/kdaudt/gomaildrop/maildir"
)

type Args struct {
	mailFilterFile string
	testConfig     bool
}

func main() {
	var args Args
	flag.StringVar(&args.mailFilterFile, "filter", "", "The location of the mailfilter file, defaults to $HOME/.mailfilter")
	flag.BoolVar(&args.testConfig, "test", false, "Test the configuration and show the actions")
	flag.Parse()

	homedir, err := os.UserHomeDir()
	if err != nil {
		returnError(err)
	}

	buf := bytes.Buffer{}
	_, err = buf.ReadFrom(os.Stdin)

	if err != nil {
		returnError(err)
	}

	// Skip first line, as the mailbox From line is not a valid mime header
	bufBytes := buf.Bytes()
	if len(bufBytes) >= 5 && string(bufBytes[0:5]) == "From " {
		_, err = buf.ReadBytes('\n')
		if err != nil {
			returnError(err)
		}
	}

	reader := bytes.NewReader(buf.Bytes())

	var filterFile *os.File
	if args.mailFilterFile == "" {
		filterFile, err = os.Open(path.Join(homedir, ".mailfilter"))
	} else {
		filterFile, err = os.Open(args.mailFilterFile)
	}

	if err != nil {
		returnError(err)
	}

	message, err := mail.ReadMessage(reader)
	if err != nil {
		returnError(err)
	}

	actions, err := filter.Filter(message, filterFile)
	if err != nil {
		returnError(err)
	}

	err = os.Chdir(homedir)
	if err != nil {
		returnError(err)
	}

	for _, action := range actions {
		_, err = reader.Seek(0, io.SeekStart)
		if err != nil {
			returnError(err)
		}

		if args.testConfig {
			fmt.Printf("action: %s\n", action.Maildir.Dir)
		} else {
			err = maildir.WriteMessage(reader, action.Maildir)
			if err != nil {
				returnError(err)
			}
		}

	}
}

func returnError(message any) {
	fmt.Fprintf(os.Stderr, "error: %s\n", message)
	os.Exit(1)
}
