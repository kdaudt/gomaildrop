package filter

import (
	"context"
	"fmt"
	"io"
	"net/mail"
	"os"
	"regexp"
	"strings"

	"gitlab.alpinelinux.org/kdaudt/gomaildrop/maildir"
	"mvdan.cc/sh/v3/expand"
	"mvdan.cc/sh/v3/interp"
	"mvdan.cc/sh/v3/syntax"
)

type FilterAction struct {
	Maildir maildir.Maildir
}

type Environ map[string]string

func (e Environ) Get(name string) (variable expand.Variable) {
	if val, ok := e[name]; ok {
		variable.Str = val
		variable.Kind = expand.String
	} else {
		variable.Kind = expand.Unset
	}
	return
}

func (e Environ) Each(fn func(name string, vr expand.Variable) bool) {
	for name := range e {
		if !fn(name, e.Get(name)) {
			break
		}
	}
}

func Filter(msg *mail.Message, filter io.Reader) (actions []FilterAction, err error) {
	if msg == nil {
		err = fmt.Errorf("expected message to be provided")
		return
	}
	parser := syntax.NewParser()

	ast, err := parser.Parse(filter, ".mailfilter")
	if err != nil {
		return
	}

	_, err = interp.New(
		interp.ExecHandler(func(ctx context.Context, args []string) error {
			return fmt.Errorf("not allowed to run external commands")
		}),
		interp.OpenHandler(func(ctx context.Context, path string, flag int, perm os.FileMode) (io.ReadWriteCloser, error) {
			return nil, fmt.Errorf("opening files is not supported")
		}),
		interp.Env(expand.ListEnviron()),
	)

	env := Environ{}
	for name, values := range msg.Header {
		var_name := strings.ToLower(name)
		var_name = strings.ReplaceAll(var_name, "-", "_")

		env[var_name] = strings.Join(values, " ")
	}

	cfg := &expand.Config{
		Env: env,
	}

	stopWalk := false
	var walkErr error
	var walk func(n syntax.Node) bool
	walk = func(n syntax.Node) bool {
		if stopWalk || walkErr != nil {
			return false
		}

		switch node := n.(type) {
		case *syntax.File:
			return true
		case *syntax.Stmt:
			return true
		case *syntax.IfClause:
			result, walkErr := evaluateIfClause(node.Cond, cfg)
			if walkErr != nil {
				return false
			}
			if result {
				for _, stmt := range node.Then {
					syntax.Walk(stmt, walk)
				}
			} else if node.Else != nil {
				syntax.Walk(node.Else, walk)
			}

			return false
		case *syntax.CallExpr:
			command := node.Args[0]
			commandName, _ := expand.Literal(cfg, command)

			args := node.Args[1:]

			switch commandName {
			case "to":
				var to string
				to, err = expand.Literal(nil, args[0])
				if err != nil {
					stopWalk = true
					return false
				}
				action := FilterAction{
					Maildir: maildir.Maildir{Dir: to},
				}
				actions = append(actions, action)
				stopWalk = true
				return false
			case "echo":
				var output string
				output, err := expand.Literal(cfg, args[0])
				if err != nil {
					stopWalk = true
					return false
				}

				fmt.Println(output)
			default:
				err = fmt.Errorf("unknown command: %s", commandName)
			}
			return false
		case nil:
			return false
		default:
			stopWalk = true
			return false
		}
	}

	syntax.Walk(ast, walk)
	if walkErr != nil {
		err = walkErr
	}

	return
}

func evaluateIfClause(conditions []*syntax.Stmt, cfg *expand.Config) (result bool, err error) {
	result = true
	for _, stmt := range conditions {
		negateIfNeeded := func(v bool) bool {
			return stmt.Negated && !v || !stmt.Negated && v
		}

		switch s := stmt.Cmd.(type) {
		case *syntax.BinaryCmd:
			resX, err := evaluateIfClause([]*syntax.Stmt{s.X}, cfg)
			if err != nil {
				return false, err
			}
			resY, err := evaluateIfClause([]*syntax.Stmt{s.Y}, cfg)
			if err != nil {
				return false, err
			}

			switch s.Op {
			case syntax.AndStmt:
				return resX && resY, nil
			case syntax.OrStmt:
				return resX || resY, nil
			}
		case *syntax.CallExpr:
			subResult, err := evaluateIfCallExpression(s, cfg)
			if err != nil {
				return false, err
			}
			result = result && negateIfNeeded(subResult)
		}
	}

	return
}

func evaluateIfCallExpression(stmt *syntax.CallExpr, cfg *expand.Config) (bool, error) {
	cmd, args := deconstructCallExpr(stmt.Args, cfg)
	switch cmd {
	case "[":
		var operator, x, y string
		if len(args) > 0 && args[len(args)-1] == "]" {
			args = args[0 : len(args)-1]
		}
		switch len(args) {
		case 2:
			operator = args[0]
			x = args[1]
		case 3:
			operator = args[1]
			x = args[0]
			y = args[2]
		default:
			err := fmt.Errorf("cannot evaluate ifClause, unsupported number of arguments: [%s]", strings.Join(args, ", "))
			return false, err
		}

		switch operator {
		case "=":
			return x == y, nil
		case "!=":
			return x != y, nil
		case "~":
			pattern, err := regexp.Compile(y)
			if err != nil {
				return false, fmt.Errorf("failed to compile regular expressions '%s', %w", y, err)
			}

			return pattern.MatchString(x), nil
		case "!~":
			pattern, err := regexp.Compile(y)
			if err != nil {
				return false, fmt.Errorf("failed to compile regular expressions '%s', %w", y, err)
			}

			return !pattern.MatchString(x), nil
		}
	default:
		err := fmt.Errorf("unsupported command for ifClause: %s", cmd)
		return false, err
	}

	return false, fmt.Errorf("unexpected fallthrough")
}

func deconstructCallExpr(words []*syntax.Word, cfg *expand.Config) (command string, args []string) {
	for i, word := range words {
		expanded, _ := expand.Literal(cfg, word)
		if i == 0 {
			command = expanded
		} else {
			args = append(args, expanded)
		}
	}
	return
}
