package filter

import (
	"fmt"
	"net/mail"
	"strings"
	"testing"

	"github.com/MakeNowJust/heredoc"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"mvdan.cc/sh/v3/expand"
	"mvdan.cc/sh/v3/syntax"
)

func TestFilterHandlesSingleToStatement(t *testing.T) {
	filter := strings.NewReader(`to Mail/mailing-list`)

	msg := &mail.Message{}

	actions, err := Filter(msg, filter)

	require.Nil(t, err)
	require.Len(t, actions, 1, "Expected 1 action to be returned")

	assert.Equal(t, "Mail/mailing-list", actions[0].Maildir.Dir)
}

func TestFilterStopsHandlingAfterToStatement(t *testing.T) {
	filter := strings.NewReader(heredoc.Doc(`
		to Mail/mailing-list
		to foobar
	`))

	msg := &mail.Message{}

	actions, err := Filter(msg, filter)

	require.Nil(t, err)
	require.Len(t, actions, 1, "Expected no more than 1 command to be returned")
}

func TestFilterRejectsUnknownCommands(t *testing.T) {
	filter := strings.NewReader(`foo Mail/mailing-list`)

	actions, err := Filter(nil, filter)

	require.NotNil(t, err)
	require.Len(t, actions, 0)
}

func TestFilterSupportsIfClauseWithOperators(t *testing.T) {
	cases := []struct {
		Description string
		Example     string
		To          string
		Maildir     string
	}{
		{
			Description: "= operator when condition matches",
			Example: heredoc.Doc(`
				if [ "$to" = "mailing@list.org" ]; then
					to Mail/mailing-list
				fi
				to fallback
			`),
			To:      "mailing@list.org",
			Maildir: "Mail/mailing-list",
		},
		{
			Description: "= operator when condition does not match",
			Example: heredoc.Doc(`
				if [ "$to" = "mailing@list.org" ]; then
					to Mail/mailing-list
				fi
				to fallback
			`),
			To:      "another@list.org",
			Maildir: "fallback",
		},
		{
			Description: "= operator when condition does not match",
			Example: heredoc.Doc(`
				if [ "$to" = "mailing@list.org" ]; then
					to Mail/mailing-list
				fi
				to fallback
			`),
			To:      "another@list.org",
			Maildir: "fallback",
		},
		{
			Description: "Negated condition",
			Example: heredoc.Doc(`
				if ! [ "$to" = "mailing@list.org" ]; then
					to Mail/spam
				fi
				to Mail/mailing-list
			`),
			To:      "random@list.org",
			Maildir: "Mail/spam",
		},
		{
			Description: "!= operator",
			Example: heredoc.Doc(`
				if [ "$to" != "mailing@list.org" ]; then
					to Mail/spam
				fi
				to Mail/mailing-list
			`),
			To:      "random@list.org",
			Maildir: "Mail/spam",
		},
		{
			Description: "~ operator when matching",
			Example: heredoc.Doc(`
				if [ "$to" ~ ".*@list.org" ]; then
					to Mail/list
				fi
				to Mail/default
			`),
			To:      "foo@list.org",
			Maildir: "Mail/list",
		},
		{
			Description: "~ operator when not matching",
			Example: heredoc.Doc(`
				if [ "$to" ~ ".*@list.org" ]; then
					to Mail/list
				fi
				to Mail/default
			`),
			To:      "bar@otherlist.org",
			Maildir: "Mail/default",
		},
		{
			Description: "!~ operator when matching",
			Example: heredoc.Doc(`
				if [ "$to" !~ ".*@list.org" ]; then
					to Mail/spam
				fi
				to Mail/list
			`),
			To:      "mailing@list.org",
			Maildir: "Mail/list",
		},
		{
			Description: "!~ operator when not matching",
			Example: heredoc.Doc(`
				if [ "$to" !~ ".*@list.org" ]; then
					to Mail/spam
				fi
				to Mail/list
			`),
			To:      "buy@more.org",
			Maildir: "Mail/spam",
		},
	}

	for _, testcase := range cases {
		t.Run(testcase.Description, func(t *testing.T) {
			filter := strings.NewReader(testcase.Example)
			message := &mail.Message{
				Header: mail.Header{
					"To": {testcase.To},
				},
			}

			actions, err := Filter(message, filter)

			require.Nil(t, err)
			require.Len(t, actions, 1)

			action := actions[0]
			assert.Equal(t, testcase.Maildir, action.Maildir.Dir)
		})
	}
}

func TestFilterSupportsIfElseClause(t *testing.T) {
	filter := strings.NewReader(heredoc.Doc(`
		if [ "$to" = "mailing@list.org" ]; then
			to Mail/mailing-list
		else
			to Mail/fallback
		fi
	`))

	msg := &mail.Message{
		Header: mail.Header{
			"To": {"other@list.org"},
		},
	}

	actions, err := Filter(msg, filter)

	require.Nil(t, err)
	require.Len(t, actions, 1, "Expected 1 command to be returned")

	action := actions[0]
	assert.Equal(t, "Mail/fallback", action.Maildir.Dir)
}

func TestFilterSupportsArbitraryEmailHeaderFields(t *testing.T) {
	cases := []struct {
		Description string
		Example     string
		Header      mail.Header
		Maildir     string
	}{
		{
			Description: "Subject header",
			Example: heredoc.Doc(`
				if [ "$subject" = "[SPAM]" ]; then
					to Mail/spam
				fi
				to fallback
			`),
			Header:  mail.Header{"Subject": {"[SPAM]"}},
			Maildir: "Mail/spam",
		},
		{
			Description: "List-ID header",
			Example: heredoc.Doc(`
				if [ "$list_id" = "A Mailing list <mailing@list.org>" ]; then
					to Mail/mailing-list
				fi
				to fallback
			`),
			Header:  mail.Header{"List-ID": {"A Mailing list <mailing@list.org>"}},
			Maildir: "Mail/mailing-list",
		},
	}

	for _, testcase := range cases {
		t.Run(testcase.Description, func(t *testing.T) {
			filter := strings.NewReader(testcase.Example)
			message := &mail.Message{
				Header: testcase.Header,
			}

			actions, err := Filter(message, filter)

			require.Nil(t, err)
			require.Len(t, actions, 1)

			action := actions[0]
			assert.Equal(t, testcase.Maildir, action.Maildir.Dir)
		})
	}
}

func parseIfExpression(expression string) (stmts []*syntax.Stmt, err error) {
	parser := syntax.NewParser()

	ast, err := parser.Parse(strings.NewReader(expression), "")
	if err != nil {
		return
	}

	ifClause, ok := ast.Stmts[0].Cmd.(*syntax.IfClause)
	if !ok {
		return nil, fmt.Errorf("Expected first statement to be an ifClause, received %T", ast.Stmts[0].Cmd)
	}

	return ifClause.Cond, nil
}

func TestEvaluateIfClauseHandlesBooleanExpressions(t *testing.T) {
	cases := []struct {
		Description string
		Expression  string
		Expected    bool
		Env         expand.Environ
	}{
		{
			Description: "&& expression that evaluates to true",
			Expression: heredoc.Doc(`
				if [ "$a" = 1 ] && [ "$b" = 2 ]; then
					:
				fi
			`),
			Env:      expand.ListEnviron("a=1", "b=2"),
			Expected: true,
		},
		{
			Description: "&& expression that evaluates to false",
			Expression: heredoc.Doc(`
				if [ "$a" = 1 ] && [ "$b" = 2 ]; then
					:
				fi
			`),
			Env:      expand.ListEnviron("a=1", "b=1"),
			Expected: false,
		},
		{
			Description: "|| expression that evaluates to true",
			Expression: heredoc.Doc(`
				if [ "$a" = 1 ] || [ "$b" = 2 ]; then
					:
				fi
			`),
			Env:      expand.ListEnviron("a=1", "b=1"),
			Expected: true,
		},
		{
			Description: "|| expression that evaluates to false",
			Expression: heredoc.Doc(`
				if [ "$a" = 1 ] || [ "$b" = 2 ]; then
					:
				fi
			`),
			Env:      expand.ListEnviron("a=2", "b=1"),
			Expected: false,
		},
		{
			Description: "combined || and && expression that evaluates to true",
			Expression: heredoc.Doc(`
				if [ "$a" = 1 ] || [ "$b" = 1 ] && [ "$c" = 3 ]; then
					:
				fi
			`),
			Env:      expand.ListEnviron("a=2", "b=1", "c=3"),
			Expected: true,
		},
		{
			Description: "combined || and && expression that evaluates to false",
			Expression: heredoc.Doc(`
				if [ "$a" = 1 ] || [ "$b" = 1 ] && [ "$c" = 3 ]; then
					:
				fi
			`),
			Env:      expand.ListEnviron("a=2", "b=1", "c=1"),
			Expected: false,
		},
	}

	for _, testcase := range cases {
		t.Run(testcase.Description, func(t *testing.T) {
			conditions, err := parseIfExpression(testcase.Expression)
			require.Nil(t, err)

			cfg := &expand.Config{
				Env: testcase.Env,
			}

			result, err := evaluateIfClause(conditions, cfg)
			require.Nil(t, err)

			assert.Equal(t, testcase.Expected, result)
		})
	}

}
