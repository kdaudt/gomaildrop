# gomaildrop

Local delivery of e-mail in maildirs based on filters.

## Building

``` sh
./configure --prefix /usr
redo build
redo check
# Provide DESTDIR in environment to install in a custom location
redo install
```

## Usage

Make sure the destination maildirs already exist. The directories must at least
have `new` and `tmp` directories. Create a `$HOME/.mailfilter` file for the
local user that will receive the e-mails.

Mail filter should have at least one unconditional `to` directive. For example:

```
to Mail/default
```

would deliver all e-mails to Mail/default


## Usage with postfix

Configure postfix to deliver e-mails to a local user. Refer to the postfix
documentation on how to do that.

Then add the following line to `main.cf`:

```
mailbox_command = /usr/bin/gomaildrop
```

Postfix will execute gomaildrop for each e-mail it needs to deliver as the
recipient user.
