package maildir

import (
	"errors"
	"fmt"
	"io"
	"os"
	"path"
	"strings"
	"syscall"
	"time"
)

var ErrRetriesExhausted = fmt.Errorf("exhausted retries finding a unique filename")

type (
	MessageInfo struct {
		Time     time.Time
		Hostname string
		Pid      int
		File     os.FileInfo
		Unique   *int
	}

	Maildir struct {
		Dir string
	}
)

func (md Maildir) Tmp() string {
	return path.Join(md.Dir, "tmp")
}

func (md Maildir) New() string {
	return path.Join(md.Dir, "new")
}

func (md Maildir) Cur() string {
	return path.Join(md.Dir, "cur")
}

func WriteMessage(message io.Reader, dst Maildir) error {
	// 1: Write mail to temporary file in maildir/tmp
	f, err := os.CreateTemp(dst.Tmp(), "gomaildrop")
	if err != nil {
		return err
	}
	defer f.Close()
	_, err = f.ReadFrom(message)
	if err != nil {
		return err
	}

	// 2: Determine the new filename

	stat, err := f.Stat()
	if err != nil {
		return err
	}

	hostname, err := os.Hostname()
	if err != nil {
		return err
	}

	var newFilename string
	for i := 0; i < 10; i++ {
		messageInfo := MessageInfo{
			Time:     time.Now(),
			Hostname: hostname,
			Pid:      os.Getpid(),
			File:     stat,
			Unique:   &i,
		}

		newFilename, err = MaildirNameForMessage(messageInfo)

		if err != nil && !errors.Is(err, os.ErrExist) {
			return err
		}
	}
	if err != nil {
		if errors.Is(err, os.ErrExist) {
			return ErrRetriesExhausted
		} else {
			return err
		}
	}

	// 3: Move / rename the file in maildir/tmp to maildir/new
	return os.Rename(f.Name(), path.Join(dst.New(), newFilename))
}

func MaildirNameForMessage(msginfo MessageInfo) (string, error) {
	if msginfo.File == nil {
		return "", fmt.Errorf("MessageInfo.File cannot be nil")
	}

	filename := strings.Builder{}
	filename.WriteString(fmt.Sprint(msginfo.Time.Unix()))

	filename.WriteByte('.')
	filename.WriteString(fmt.Sprintf("M%d", msginfo.Time.Nanosecond()/1000))
	filename.WriteString(fmt.Sprintf("P%d", msginfo.Pid))

	if stat, ok := msginfo.File.Sys().(*syscall.Stat_t); ok {
		filename.WriteString(fmt.Sprintf("V%016X", stat.Dev))
		filename.WriteString(fmt.Sprintf("I%016X", stat.Ino))
	}

	if msginfo.Unique != nil {
		filename.WriteString(fmt.Sprintf("_%d", *msginfo.Unique))
	}

	filename.WriteByte('.')
	filename.WriteString(msginfo.Hostname)

	filename.WriteByte(',')
	filename.WriteString(fmt.Sprintf("S=%d", msginfo.File.Size()))

	return filename.String(), nil
}
