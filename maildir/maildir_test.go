package maildir

import (
	"io/fs"
	"syscall"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

type FileInfo struct {
	name    string
	size    int64
	mode    fs.FileMode
	modTime time.Time
	isDir   bool
	sys     any
}

func (fi FileInfo) Name() string {
	return fi.name
}

func (fi FileInfo) Size() int64 {
	return fi.size
}

func (fi FileInfo) Mode() fs.FileMode {
	return fi.mode
}

func (fi FileInfo) ModTime() time.Time {
	return fi.modTime
}

func (fi FileInfo) IsDir() bool {
	return fi.isDir
}

func (fi FileInfo) Sys() any {
	return fi.sys
}

func TestMaildirNameForMessageUsesPrimaryInfo(t *testing.T) {
	mi := MessageInfo{
		Time:     time.Date(2022, 1, 2, 3, 4, 5, 123456000, time.FixedZone("UTC", 0)),
		Pid:      123,
		Hostname: "machine",
		File:     FileInfo{size: 4567},
	}

	expected := "1641092645.M123456P123.machine,S=4567"
	actual, err := MaildirNameForMessage(mi)
	require.Nil(t, err)
	assert.Equal(t, expected, actual)
}

func TestMailDirNameForMessageUsesFileInfoIfAvailable(t *testing.T) {
	fileStat := &syscall.Stat_t{
		Ino: 12831,
		Dev: 64768,
	}
	fileInfo := FileInfo{
		size: 4567,
		sys:  fileStat,
	}

	mi := MessageInfo{
		Time:     time.Date(2022, 1, 2, 3, 4, 5, 123456000, time.FixedZone("UTC", 0)),
		Pid:      123,
		Hostname: "machine",
		File:     fileInfo,
	}

	expected := "1641092645.M123456P123V000000000000FD00I000000000000321F.machine,S=4567"
	actual, err := MaildirNameForMessage(mi)
	require.Nil(t, err)
	assert.Equal(t, expected, actual)
}

func TestMaildirNameForMessageUsesUniqueField(t *testing.T) {
	unique := 0
	mi := MessageInfo{
		Time:     time.Date(2022, 1, 2, 3, 4, 5, 123456000, time.FixedZone("UTC", 0)),
		Pid:      123,
		Hostname: "machine",
		File:     FileInfo{size: 4567},
		Unique:   &unique,
	}

	expected := "1641092645.M123456P123_0.machine,S=4567"
	actual, err := MaildirNameForMessage(mi)
	require.Nil(t, err)

	assert.Equal(t, expected, actual)
}
