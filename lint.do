exec >&2

set -o pipefail

issues=false

prefix() {
    checker=$1
    sed "s/^/$checker: /"
}

check_fmt() {
    unformatted_files=$(gofmt -l .)
    if [ -n "$unformatted_files" ]; then
        echo "$unformatted_files"
        return 1
    fi
}

staticcheck ./... | prefix staticcheck || issues=true
errcheck -exclude .errcheckignore ./... | prefix errcheck || issues=true
check_fmt | prefix gofmt || issues=true

if $issues; then
    exit 1
fi
