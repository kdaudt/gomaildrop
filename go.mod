module gitlab.alpinelinux.org/kdaudt/gomaildrop

go 1.18

require (
	github.com/MakeNowJust/heredoc v1.0.0
	github.com/spf13/pflag v1.0.5
	github.com/stretchr/testify v1.8.1
	mvdan.cc/sh/v3 v3.6.0
)

require (
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/pmezard/go-difflib v1.0.0 // indirect
	golang.org/x/sync v0.1.0 // indirect
	golang.org/x/sys v0.3.0 // indirect
	golang.org/x/term v0.3.0 // indirect
	gopkg.in/yaml.v3 v3.0.1 // indirect
)
